# code assessment

Install dependency:
> make install

In order to test:
> make test

In order to run:
> docker-compose up
