package main

import (
	"net/http"

	"github.com/rs/zerolog/log"

	"bitbucket.org/papazz/code-assessment/api"
	"bitbucket.org/papazz/code-assessment/database"
	"bitbucket.org/papazz/code-assessment/database/sql"
	"bitbucket.org/papazz/code-assessment/service"
)

func main() {
	builder := initBuilder()
	serv, err := builder.Build()
	if err != nil {
		log.Fatal().Msgf("could not build service: %s", err)
	}

	log.Info().Msg("service started")
	if err := http.ListenAndServe(":8080", api.New(serv)); err != nil {
		log.Fatal().
			Err(err).
			Msg("service starting failed")
	}
}

func initBuilder() *service.Builder {
	db, err := database.InitPGSQLDB()
	if err != nil {
		log.Fatal().Msgf("could not connect to database: %s", err)
	}
	provider := sql.New(db)

	builder := service.NewBuilder()
	builder.
		Provider(provider)
	return builder
}
