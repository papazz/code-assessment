package api

import (
	"net/http"
	"testing"

	"github.com/gavv/httpexpect"
)

func TestNew(t *testing.T) {
	tt := []struct {
		route  string
		method string
		query  string
		status int
	}{
		{
			route:  "/healthz",
			method: http.MethodGet,
			status: http.StatusOK,
		},
		{
			route:  "/api/v1/companies",
			method: http.MethodGet,
			status: http.StatusInternalServerError,
		},
		{
			route:  "/api/v1/companies",
			method: http.MethodPost,
			status: http.StatusBadRequest,
		},
		{
			route:  "/api/v1/companies",
			method: http.MethodPut,
			status: http.StatusNotFound,
		},
		{
			route:  "/api/v1/companies",
			method: http.MethodPatch,
			status: http.StatusNotFound,
		},
		{
			route:  "/api/v1/companies",
			method: http.MethodDelete,
			status: http.StatusNotFound,
		},
		{
			route:  "/api/v1/companies/1",
			method: http.MethodGet,
			status: http.StatusInternalServerError,
		},
		{
			route:  "/api/v1/companies/1",
			method: http.MethodPut,
			status: http.StatusNotFound,
		},
		{
			route:  "/api/v1/companies/1",
			method: http.MethodPatch,
			status: http.StatusNotFound,
		},
		{
			route:  "/api/v1/companies/1",
			method: http.MethodDelete,
			status: http.StatusInternalServerError,
		},
		{
			route:  "/api/v1/contracts",
			method: http.MethodGet,
			status: http.StatusInternalServerError,
		},
		{
			route:  "/api/v1/contracts",
			method: http.MethodPost,
			status: http.StatusBadRequest,
		},
		{
			route:  "/api/v1/contracts",
			method: http.MethodPut,
			status: http.StatusNotFound,
		},
		{
			route:  "/api/v1/contracts",
			method: http.MethodPatch,
			status: http.StatusNotFound,
		},
		{
			route:  "/api/v1/contracts",
			method: http.MethodDelete,
			status: http.StatusNotFound,
		},
		{
			route:  "/api/v1/contracts/1",
			method: http.MethodGet,
			status: http.StatusInternalServerError,
		},
		{
			route:  "/api/v1/contracts/1",
			method: http.MethodDelete,
			status: http.StatusInternalServerError,
		},
		{
			route:  "/api/v1/contracts/1",
			method: http.MethodPut,
			status: http.StatusNotFound,
		},
		{
			route:  "/api/v1/contracts/1",
			method: http.MethodPatch,
			status: http.StatusNotFound,
		},
		{
			route:  "/api/v1/contracts/1",
			method: http.MethodPost,
			status: http.StatusNotFound,
		}, {
			route:  "/api/v1/purchases",
			method: http.MethodPost,
			status: http.StatusBadRequest,
		},
	}

	h := New(nil)
	for _, tc := range tt {
		t.Run(tc.route, func(t *testing.T) {
			e := httpexpect.WithConfig(httpexpect.Config{
				Client: &http.Client{
					Transport: httpexpect.NewBinder(h),
					Jar:       httpexpect.NewJar(),
				},
				Reporter: httpexpect.NewAssertReporter(t),
			})
			e.Request(tc.method, tc.route).
				WithQueryString(tc.query).
				Expect().
				Status(tc.status)
		})
	}
}
