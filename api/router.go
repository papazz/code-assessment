package api

import (
	"net/http"

	"bitbucket.org/papazz/code-assessment/entity"
	"github.com/gin-gonic/gin"
)

// Handler provides routing and request handling.
type (
	Handler struct {
		*gin.Engine
		service entity.Service
	}
)

// New registers all api paths to corresponding handlers
// and returns Handler capable of serving incoming requests
func New(service entity.Service) Handler {
	// create the framework engine
	router := gin.New()
	// attach Logger
	router.Use(gin.Logger())
	// attach Recovery middleware
	router.Use(gin.Recovery())

	api := Handler{
		router,
		service,
	}
	// health check endpoint
	api.GET("/healthz", func(ctx *gin.Context) {
		ctx.JSON(
			http.StatusOK,
			struct {
				Status string
			}{Status: "ok"})
	})
	// companies router
	v1 := api.Group("/api/v1")
	{
		v1.GET("/companies", api.companies)
		v1.GET("/companies/:id", api.company)
		v1.POST("/companies", api.createCompany)
		v1.DELETE("/companies/:id", api.deleteCompany)
	}
	// contracts router
	{
		v1.GET("/contracts", api.contracts)
		v1.GET("/contracts/:id", api.contract)
		v1.POST("/contracts", api.createContract)
		v1.DELETE("/contracts/:id", api.deleteContract)
	}
	// purchases router
	{
		v1.POST("/purchases", api.doPurchase)
	}
	return api
}
