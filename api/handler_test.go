package api

import (
	"fmt"
	"net/http"
	"testing"

	"bitbucket.org/papazz/code-assessment/entity"
	"github.com/gavv/httpexpect"
	"github.com/stretchr/testify/mock"
)

var errMock = fmt.Errorf("")

func TestHandler_companies(t *testing.T) {
	mockService := new(MockService)
	defer mockService.AssertExpectations(t)

	mockService.On("Companies", mock.Anything).Return([]entity.Company{
		{
			Name:             t.Name(),
			RegistrationCode: t.Name(),
		},
	}, nil)

	mockService.On("Companies", mock.Anything).Return([]entity.Company{}, nil)
	tt := []struct {
		name         string
		expectBody   string
		id           int64
		expectStatus int
	}{
		{
			name:         "ok",
			id:           int64(0),
			expectStatus: http.StatusOK,
		},
	}

	h := New(mockService)
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			exp := httpexpect.WithConfig(httpexpect.Config{
				Client: &http.Client{
					Transport: httpexpect.NewBinder(h),
					Jar:       httpexpect.NewJar(),
				},
				Reporter: httpexpect.NewAssertReporter(t),
			})
			exp.GET("/api/v1/companies").
				Expect().
				Status(tc.expectStatus)
		})
	}
}

func TestHandler_company(t *testing.T) {
	mockService := new(MockService)
	defer mockService.AssertExpectations(t)

	mockService.On("Company", mock.Anything, int64(0)).Return(&entity.Company{}, nil)
	mockService.On("Company", mock.Anything, int64(1)).Return(&entity.Company{
		Name:             t.Name(),
		RegistrationCode: t.Name(),
	}, nil)
	mockService.On("Company", mock.Anything, int64(2)).Return(nil, fmt.Errorf("could not fetch company by ID: %s", errMock))

	tt := []struct {
		name         string
		id           int64
		expectBody   string
		expectStatus int
	}{
		{
			name:         "database call failed",
			id:           int64(2),
			expectStatus: http.StatusInternalServerError,
		},
		{
			name:         "ok empty",
			id:           int64(0),
			expectBody:   "{\"data\":{},\"status\":200}",
			expectStatus: http.StatusOK,
		},
		{
			name:         "ok result",
			id:           int64(1),
			expectBody:   "{\"data\":{\"name\":\"TestHandler_company\",\"registration_code\":\"TestHandler_company\"},\"status\":200}",
			expectStatus: http.StatusOK,
		},
	}

	h := New(mockService)
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			exp := httpexpect.WithConfig(httpexpect.Config{
				Client: &http.Client{
					Transport: httpexpect.NewBinder(h),
					Jar:       httpexpect.NewJar(),
				},
				Reporter: httpexpect.NewAssertReporter(t),
			})
			exp.GET(fmt.Sprintf("/api/v1/companies/%d", tc.id)).
				Expect().
				Status(tc.expectStatus).
				Body().Equal(tc.expectBody)
		})
	}
}

func TestHandler_createCompany(t *testing.T) {
	mockService := new(MockService)
	defer mockService.AssertExpectations(t)

	mockService.On("CreateCompany", mock.Anything, &entity.Company{
		Name:             t.Name(),
		RegistrationCode: t.Name(),
	}).Return(int64(0), nil)

	tt := []struct {
		name         string
		id           int64
		body         string
		expectStatus int
	}{
		{
			name:         "ok",
			body:         fmt.Sprintf("{\"registration_code\": \"%s\", \"name\": \"%s\"}", t.Name(), t.Name()),
			expectStatus: http.StatusCreated,
		}, {
			name:         "error binding name",
			body:         fmt.Sprintf("{\"registration_code\": \"%s\", \"name\": \"\"}", t.Name()),
			expectStatus: http.StatusBadRequest,
		}, {
			name:         "error binding reqistration_code",
			body:         fmt.Sprintf("{\"registration_code\": \"\", \"name\": \"%s\"}", t.Name()),
			expectStatus: http.StatusBadRequest,
		}, {
			name:         "error binding fields of body",
			body:         "{}",
			expectStatus: http.StatusBadRequest,
		}, {
			name:         "ok 2",
			body:         fmt.Sprintf("{\"registration_code\": \"%s\", \"name\": \"%s\", \"val\": \"1\"}", t.Name(), t.Name()),
			expectStatus: http.StatusCreated,
		},
	}

	h := New(mockService)
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			exp := httpexpect.WithConfig(httpexpect.Config{
				Client: &http.Client{
					Transport: httpexpect.NewBinder(h),
					Jar:       httpexpect.NewJar(),
				},
				Reporter: httpexpect.NewAssertReporter(t),
			})
			exp.POST("/api/v1/companies").
				WithBytes([]byte(tc.body)).
				Expect().
				Status(tc.expectStatus)
		})
	}
}

func TestHandler_deleteCompany(t *testing.T) {
	mockService := new(MockService)
	defer mockService.AssertExpectations(t)

	mockService.On("DeleteCompany", mock.Anything, int64(1)).Return(int64(1), nil)
	mockService.On("DeleteCompany", mock.Anything, int64(0)).Return(int64(-1), errMock)

	tt := []struct {
		name         string
		id           int64
		expectStatus int
	}{
		{
			name:         "ok",
			id:           int64(1),
			expectStatus: http.StatusNoContent,
		}, {
			name:         "error no ID",
			id:           int64(0),
			expectStatus: http.StatusNotFound,
		},
	}

	h := New(mockService)
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			exp := httpexpect.WithConfig(httpexpect.Config{
				Client: &http.Client{
					Transport: httpexpect.NewBinder(h),
					Jar:       httpexpect.NewJar(),
				},
				Reporter: httpexpect.NewAssertReporter(t),
			})
			exp.DELETE(fmt.Sprintf("/api/v1/companies/%d", tc.id)).
				Expect().
				Status(tc.expectStatus)
		})
	}
}

func TestHandler_contracts(t *testing.T) {
	mockService := new(MockService)
	defer mockService.AssertExpectations(t)

	mockService.On("Contracts", mock.Anything).Return([]entity.Contract{
		{
			SellerID:               int64(1),
			ClientID:               int64(2),
			ContractNumber:         "ABC123",
			DateSigned:             "2020-01-02T15:04:05Z07:00",
			ValidTill:              "2020-01-02T15:04:05Z07:01",
			CreditsInitiallyIssued: int64(100),
		},
	}, nil)

	mockService.On("Contracts", mock.Anything).Return([]entity.Company{}, nil)
	tt := []struct {
		name         string
		expectBody   string
		expectStatus int
	}{
		{
			name:         "ok",
			expectBody:   "{\"data\":[{\"seller_id\":1,\"client_id\":2,\"contract_number\":\"ABC123\",\"date_signed\":\"2020-01-02T15:04:05Z07:00\",\"valid_till\":\"2020-01-02T15:04:05Z07:01\",\"credits_initially_issued\":100}],\"status\":200}",
			expectStatus: http.StatusOK,
		},
	}

	h := New(mockService)
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			exp := httpexpect.WithConfig(httpexpect.Config{
				Client: &http.Client{
					Transport: httpexpect.NewBinder(h),
					Jar:       httpexpect.NewJar(),
				},
				Reporter: httpexpect.NewAssertReporter(t),
			})
			exp.GET("/api/v1/contracts").
				Expect().
				Status(tc.expectStatus).
				Body().Equal(tc.expectBody)
		})
	}
}

func TestHandler_contract(t *testing.T) {
	mockService := new(MockService)
	defer mockService.AssertExpectations(t)

	mockService.On("Contract", mock.Anything, int64(0)).Return(&entity.Contract{}, nil)
	mockService.On("Contract", mock.Anything, int64(1)).Return(&entity.Contract{
		SellerID:               int64(1),
		ClientID:               int64(2),
		ContractNumber:         "ABC123",
		DateSigned:             "2020-01-02T15:04:05Z07:00",
		ValidTill:              "2020-01-02T15:04:05Z07:01",
		CreditsInitiallyIssued: int64(100),
	}, nil)
	mockService.On("Contract", mock.Anything, int64(2)).Return(nil, fmt.Errorf("could not fetch contract by ID: %s", errMock))

	tt := []struct {
		name         string
		id           int64
		expectBody   string
		expectStatus int
	}{
		{
			name:         "database call failed",
			id:           int64(2),
			expectStatus: http.StatusInternalServerError,
		},
		{
			name:         "ok empty",
			id:           int64(0),
			expectBody:   "{\"data\":{},\"status\":200}",
			expectStatus: http.StatusOK,
		},
		{
			name:         "ok result",
			id:           int64(1),
			expectBody:   "{\"data\":{\"seller_id\":1,\"client_id\":2,\"contract_number\":\"ABC123\",\"date_signed\":\"2020-01-02T15:04:05Z07:00\",\"valid_till\":\"2020-01-02T15:04:05Z07:01\",\"credits_initially_issued\":100},\"status\":200}",
			expectStatus: http.StatusOK,
		},
	}

	h := New(mockService)
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			exp := httpexpect.WithConfig(httpexpect.Config{
				Client: &http.Client{
					Transport: httpexpect.NewBinder(h),
					Jar:       httpexpect.NewJar(),
				},
				Reporter: httpexpect.NewAssertReporter(t),
			})
			exp.GET(fmt.Sprintf("/api/v1/contracts/%d", tc.id)).
				Expect().
				Status(tc.expectStatus).
				Body().Equal(tc.expectBody)
		})
	}
}

func TestHandler_createContract(t *testing.T) {
	mockService := new(MockService)
	defer mockService.AssertExpectations(t)

	mockService.On("CreateContract", mock.Anything, &entity.Contract{
		SellerID:               int64(1),
		ClientID:               int64(2),
		ContractNumber:         "ABC123",
		DateSigned:             "2020-01-02T15:04:05Z07:00",
		ValidTill:              "2020-01-02T15:04:05Z07:01",
		CreditsInitiallyIssued: int64(100),
	}).Return(int64(0), nil)

	tt := []struct {
		name         string
		body         string
		expectStatus int
		expectBody   string
	}{
		{
			name:         "ok",
			body:         "{\"seller_id\":1,\"client_id\":2,\"contract_number\":\"ABC123\",\"date_signed\":\"2020-01-02T15:04:05Z07:00\",\"valid_till\":\"2020-01-02T15:04:05Z07:01\",\"credits_initially_issued\":100}",
			expectBody:   "{\"data\":0,\"status\":201}",
			expectStatus: http.StatusCreated,
		}, {
			name:         "error binding name",
			body:         "{}",
			expectStatus: http.StatusBadRequest,
		}, {
			name:         "error binding reqistration_code",
			body:         "{\"contract_number\":\"ABC123\",\"date_signed\":\"2020-01-02T15:04:05Z07:00\",\"valid_till\":\"2020-01-02T15:04:05Z07:01\",\"credits_initially_issued\":100}",
			expectStatus: http.StatusBadRequest,
		},
	}

	h := New(mockService)
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			exp := httpexpect.WithConfig(httpexpect.Config{
				Client: &http.Client{
					Transport: httpexpect.NewBinder(h),
					Jar:       httpexpect.NewJar(),
				},
				Reporter: httpexpect.NewAssertReporter(t),
			})
			exp.POST("/api/v1/contracts").
				WithBytes([]byte(tc.body)).
				Expect().
				Status(tc.expectStatus).
				Body().Equal(tc.expectBody)
		})
	}
}

func TestHandler_deleteContract(t *testing.T) {
	mockService := new(MockService)
	defer mockService.AssertExpectations(t)

	mockService.On("DeleteContract", mock.Anything, int64(1)).Return(int64(1), nil)
	mockService.On("DeleteContract", mock.Anything, int64(0)).Return(int64(-1), errMock)

	tt := []struct {
		name         string
		id           int64
		expectStatus int
	}{
		{
			name:         "ok",
			id:           int64(1),
			expectStatus: http.StatusNoContent,
		}, {
			name:         "error no ID",
			id:           int64(0),
			expectStatus: http.StatusNotFound,
		},
	}

	h := New(mockService)
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			exp := httpexpect.WithConfig(httpexpect.Config{
				Client: &http.Client{
					Transport: httpexpect.NewBinder(h),
					Jar:       httpexpect.NewJar(),
				},
				Reporter: httpexpect.NewAssertReporter(t),
			})
			exp.DELETE(fmt.Sprintf("/api/v1/contracts/%d", tc.id)).
				Expect().
				Status(tc.expectStatus)
		})
	}
}

func TestHandler_doPurchase(t *testing.T) {
	mockService := new(MockService)
	defer mockService.AssertExpectations(t)

	//mockService.On("DoPurchase", mock.Anything, &entity.Purchase{}).Return(int64(0), nil)
	mockService.On("DoPurchase", mock.Anything, &entity.Purchase{
		Date:        "2020-01-02T15:04:05Z07:00",
		ContractID:  int64(1),
		CreditSpent: int64(100),
	}).Return(int64(1), nil)

	tt := []struct {
		name         string
		body         string
		expectStatus int
		expectBody   string
	}{
		{
			name:         "error binding name",
			body:         "{}",
			expectBody:   "{\"error_msg\":\"Key: 'Purchase.ContractID' Error:Field validation for 'ContractID' failed on the 'required' tag\\nKey: 'Purchase.Date' Error:Field validation for 'Date' failed on the 'required' tag\\nKey: 'Purchase.CreditSpent' Error:Field validation for 'CreditSpent' failed on the 'required' tag\",\"status\":400}",
			expectStatus: http.StatusBadRequest,
		}, {
			name:         "ok",
			body:         "{\"date\":\"2020-01-02T15:04:05Z07:00\",\"contract_id\":1,\"credit_spent\":100}",
			expectBody:   "{\"data\":1,\"status\":201}",
			expectStatus: http.StatusCreated,
		},
	}

	h := New(mockService)
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			exp := httpexpect.WithConfig(httpexpect.Config{
				Client: &http.Client{
					Transport: httpexpect.NewBinder(h),
					Jar:       httpexpect.NewJar(),
				},
				Reporter: httpexpect.NewAssertReporter(t),
			})
			exp.POST("/api/v1/purchases").
				WithBytes([]byte(tc.body)).
				Expect().
				Status(tc.expectStatus).
				Body().Equal(tc.expectBody)
		})
	}
}
