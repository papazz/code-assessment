package api

import (
	"bitbucket.org/papazz/code-assessment/entity"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/mock"
)

type MockService struct {
	mock.Mock
}

func (m *MockService) Companies(ctx *gin.Context) ([]entity.Company, error) {
	args := m.Called(ctx)
	return args.Get(0).([]entity.Company), args.Error(1)
}

func (m *MockService) Company(ctx *gin.Context, id int64) (*entity.Company, error) {
	args := m.Called(ctx, id)
	return args.Get(0).(*entity.Company), args.Error(1)
}

func (m *MockService) CreateCompany(ctx *gin.Context, company *entity.Company) (int64, error) {
	args := m.Called(ctx, company)
	return args.Get(0).(int64), args.Error(1)
}

func (m *MockService) DeleteCompany(ctx *gin.Context, id int64) (int64, error) {
	args := m.Called(ctx, id)
	return args.Get(0).(int64), args.Error(1)
}

func (m *MockService) Contracts(ctx *gin.Context) ([]entity.Contract, error) {
	args := m.Called(ctx)
	return args.Get(0).([]entity.Contract), args.Error(1)
}

func (m *MockService) Contract(ctx *gin.Context, id int64) (*entity.Contract, error) {
	args := m.Called(ctx, id)
	return args.Get(0).(*entity.Contract), args.Error(1)
}

func (m *MockService) CreateContract(ctx *gin.Context, contract *entity.Contract) (int64, error) {
	args := m.Called(ctx, contract)
	return args.Get(0).(int64), args.Error(1)
}

func (m *MockService) DeleteContract(ctx *gin.Context, id int64) (int64, error) {
	args := m.Called(ctx, id)
	return args.Get(0).(int64), args.Error(1)
}

func (m *MockService) DoPurchase(ctx *gin.Context, p *entity.Purchase) (int64, error) {
	args := m.Called(ctx, p)
	return args.Get(0).(int64), args.Error(1)
}
