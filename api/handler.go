package api

import (
	"net/http"
	"strconv"

	"bitbucket.org/papazz/code-assessment/entity"
	"bitbucket.org/papazz/code-assessment/util"
	"github.com/gin-gonic/gin"
)

func (h *Handler) companies(ctx *gin.Context) {
	resp, err := h.service.Companies(ctx)
	if err != nil {
		util.JSONError(ctx, http.StatusBadRequest, err.Error())
		return
	}
	util.JSONReturn(ctx, http.StatusOK, resp)
	return
}

func (h *Handler) company(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 0)
	if err != nil {
		util.JSONError(ctx, http.StatusBadRequest, err.Error())
		return
	}
	resp, err := h.service.Company(ctx, id)
	if err != nil {
		util.JSONError(ctx, http.StatusNotFound, err.Error())
		return
	}
	util.JSONReturn(ctx, http.StatusOK, resp)
	return
}

func (h *Handler) createCompany(ctx *gin.Context) {
	var c entity.Company
	ctx.BindJSON(&c)
	resp, err := h.service.CreateCompany(ctx, &c)
	if err != nil {
		util.JSONError(ctx, http.StatusBadRequest, err.Error())
		return
	}
	util.JSONReturn(ctx, http.StatusCreated, resp)
	return
}

func (h *Handler) deleteCompany(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 0)
	if err != nil {
		util.JSONError(ctx, http.StatusBadRequest, err.Error())
		return
	}
	count, err := h.service.DeleteCompany(ctx, id)
	if err != nil {
		util.JSONError(ctx, http.StatusNotFound, err.Error())
		return
	}
	if count < 1 {
		ctx.JSON(http.StatusNotFound, nil)
		return
	}
	util.JSONReturn(ctx, http.StatusNoContent, nil)
	return
}

// contracts
func (h *Handler) contracts(ctx *gin.Context) {
	resp, err := h.service.Contracts(ctx)
	if err != nil {
		util.JSONError(ctx, http.StatusNotFound, err.Error())
		return
	}
	util.JSONReturn(ctx, http.StatusOK, resp)
	return
}

func (h *Handler) contract(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 0)
	if err != nil {
		util.JSONError(ctx, http.StatusBadRequest, err.Error())
		return
	}
	resp, err := h.service.Contract(ctx, id)
	if err != nil {
		util.JSONError(ctx, http.StatusNotFound, err.Error())
		return
	}
	util.JSONReturn(ctx, http.StatusOK, resp)
	return
}

func (h *Handler) createContract(ctx *gin.Context) {
	var c entity.Contract
	ctx.BindJSON(&c)
	resp, err := h.service.CreateContract(ctx, &c)
	if err != nil {
		util.JSONError(ctx, http.StatusNotFound, err.Error())
		return
	}
	util.JSONReturn(ctx, http.StatusCreated, resp)
	return
}

func (h *Handler) deleteContract(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 0)
	if err != nil {
		util.JSONError(ctx, http.StatusBadRequest, err.Error())
		return
	}
	resp, err := h.service.DeleteContract(ctx, id)
	if err != nil {
		util.JSONError(ctx, http.StatusNotFound, err.Error())
		return
	}
	if resp < 1 {
		ctx.JSON(http.StatusNotFound, nil)
		return
	}
	util.JSONReturn(ctx, http.StatusNoContent, nil)
	return
}

func (h *Handler) doPurchase(ctx *gin.Context) {
	var p entity.Purchase
	err := ctx.BindJSON(&p)
	if err != nil {
		util.JSONError(ctx, http.StatusBadRequest, err.Error())
		return
	}
	resp, err := h.service.DoPurchase(ctx, &p)
	if err != nil {
		util.JSONError(ctx, http.StatusBadRequest, err.Error())
		return
	}
	if resp < 0 {
		util.JSONError(ctx, http.StatusBadRequest, "amount exceeds the balance")
		return
	}
	util.JSONReturn(ctx, http.StatusCreated, resp)
	return
}
