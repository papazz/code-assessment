package sql

// CreateCompany creates company.
import (
	"bitbucket.org/papazz/code-assessment/entity"
	"github.com/gin-gonic/gin"
)

// CreatePurchase creates record of purchasing.
func (p Provider) CreatePurchase(ctx *gin.Context, pu *entity.Purchase) (int64, error) {
	var balance int64
	raw := p.db.QueryRowContext(ctx, `
SELECT * FROM redeem($1, $2, $3)`, pu.ContractID, pu.CreditSpent, pu.Date)
	err := raw.Scan(&balance)
	if err != nil {
		return balance, err
	}
	return balance, nil
}
