package sql

import (
	"database/sql"
	"testing"

	"github.com/gin-gonic/gin"

	"bitbucket.org/papazz/code-assessment/database"
	"bitbucket.org/papazz/code-assessment/entity"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestProvider_FetchCompanies(t *testing.T) {
	ctx := gin.Context{}
	db, err := database.InitPGSQLDB()
	require.NoError(t, err)
	provider := New(db)

	var companyID int64
	err = provider.db.QueryRowContext(&ctx, `INSERT INTO company (name, registration_code) 
	VALUES ($1, $2) RETURNING id`, t.Name(), t.Name()).Scan(&companyID)
	assert.NoError(t, err, "could not insert company")
	assert.NotZero(t, companyID, "could not get company id")

	expectedCompanies := []entity.Company{
		{
			Name:             t.Name(),
			RegistrationCode: t.Name(),
		},
	}

	fetchedCompanies, err := provider.FetchCompanies(&ctx)
	assert.NoError(t, err, "could not fetch companies")
	assert.Equal(t, expectedCompanies, fetchedCompanies, "companies mismatch")

	_, err = provider.db.Exec(`DELETE FROM company WHERE id = $1`, companyID)
	assert.NoError(t, err, "could not remove company")
}
func TestProvider_FetchCompany(t *testing.T) {
	ctx := gin.Context{}
	db, err := database.InitPGSQLDB()
	require.NoError(t, err)
	provider := New(db)

	var companyID int64
	err = provider.db.QueryRowContext(&ctx, `INSERT INTO company (name, registration_code) 
	VALUES ($1, $2) RETURNING id`, t.Name(), t.Name()).Scan(&companyID)
	assert.NoError(t, err, "could not insert company")
	assert.NotZero(t, companyID, "could not get company id")

	expectedCompany := entity.Company{
		Name:             t.Name(),
		RegistrationCode: t.Name(),
	}

	fetchedCompany, err := provider.FetchCompany(&ctx, companyID)
	assert.NoError(t, err, "could not fetch company")
	assert.Equal(t, expectedCompany, *fetchedCompany, "company mismatch")

	_, err = provider.db.Exec(`DELETE FROM company WHERE id = $1`, companyID)
	assert.NoError(t, err, "could not remove company")
}
func TestProvider_CreateCompany(t *testing.T) {
	ctx := gin.Context{}
	db, err := database.InitPGSQLDB()
	require.NoError(t, err)
	provider := New(db)

	company := entity.Company{
		Name:             t.Name(),
		RegistrationCode: t.Name(),
	}

	createdCompanyID, err := provider.CreateCompany(&ctx, &company)
	assert.NoError(t, err, "could not create company")
	assert.NotZero(t, createdCompanyID, "company ID mismatch")

	_, err = provider.db.Exec(`DELETE FROM company WHERE id = $1`, createdCompanyID)
	assert.NoError(t, err, "could not remove company")
}

func TestProvider_DeleteCompany(t *testing.T) {
	ctx := gin.Context{}
	db, err := database.InitPGSQLDB()
	require.NoError(t, err)
	provider := New(db)

	var createdCompanyID int64
	err = provider.db.QueryRowContext(&ctx, `INSERT INTO company (name, registration_code) 
	VALUES ($1, $2) RETURNING id`, t.Name(), t.Name()).Scan(&createdCompanyID)
	assert.NoError(t, err, "could not insert company")
	assert.NotZero(t, createdCompanyID, "could not get company id")

	rowsAffected, err := provider.DeleteCompany(&ctx, createdCompanyID)
	assert.NoError(t, err, "could not delete companies")
	assert.Equal(t, int64(1), rowsAffected, "number of deleted companies mismatch")

	var i interface{}
	rowError := provider.db.QueryRowContext(&ctx, `SELECT name, registration_code FROM company WHERE id = $1`, createdCompanyID).Scan(&i)
	assert.EqualError(t, rowError, sql.ErrNoRows.Error())
}
