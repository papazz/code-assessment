package sql

import (
	"fmt"

	"bitbucket.org/papazz/code-assessment/entity"

	"github.com/gin-gonic/gin"
)

// FetchCompanies returns list of all companies.
func (p Provider) FetchCompanies(ctx *gin.Context) ([]entity.Company, error) {
	rows, err := p.db.QueryContext(ctx, fmt.Sprintf(`SELECT name, registration_code FROM company`))
	if err != nil {
		return nil, err
	}
	list := make([]entity.Company, 0)
	for rows.Next() {
		var c entity.Company
		err := rows.Scan(
			&c.Name, &c.RegistrationCode,
		)
		if err != nil {
			return nil, err
		}
		list = append(list, c)
	}
	defer rows.Close()
	return list, rows.Err()
}

// FetchCompany returns company.
func (p Provider) FetchCompany(ctx *gin.Context, id int64) (*entity.Company, error) {
	raw := p.db.QueryRowContext(ctx, fmt.Sprintf(`
SELECT name, registration_code FROM company WHERE id = %d`, id))
	var c entity.Company
	err := raw.Scan(
		&c.Name, &c.RegistrationCode,
	)
	if err != nil {
		return &c, err
	}
	return &c, nil
}

// CreateCompany creates company.
func (p Provider) CreateCompany(ctx *gin.Context, c *entity.Company) (int64, error) {
	var id int64
	err := p.db.QueryRowContext(ctx, `
INSERT INTO company (name, registration_code) 
     VALUES ($1, $2) ON CONFLICT (name) DO UPDATE SET name=EXCLUDED.name RETURNING id`, c.Name, c.RegistrationCode).Scan(&id)
	if err != nil {
		return -1, err
	}
	return id, nil
}

// DeleteCompany deletes particular company.
func (p Provider) DeleteCompany(ctx *gin.Context, id int64) (int64, error) {
	resp, err := p.db.ExecContext(ctx, `
DELETE FROM company WHERE id = $1`, id)
	count, err := resp.RowsAffected()
	if err != nil {
		return 0, err
	}
	return count, nil
}
