package sql

import (
	"bitbucket.org/papazz/code-assessment/entity"

	"github.com/gin-gonic/gin"
)

// FetchContracts returns list of all contracts.
func (p Provider) FetchContracts(ctx *gin.Context) ([]entity.Contract, error) {
	rows, err := p.db.QueryContext(ctx, `
SELECT seller_id, client_id, contract_number, 
	   date_signed, valid_till, credits_initially_issued
  FROM contract`)
	if err != nil {
		return nil, err
	}
	list := make([]entity.Contract, 0)
	for rows.Next() {
		var c entity.Contract
		err := rows.Scan(
			&c.SellerID, &c.ClientID, &c.ContractNumber,
			&c.DateSigned, &c.ValidTill, &c.CreditsInitiallyIssued,
		)
		if err != nil {
			return nil, err
		}
		list = append(list, c)
	}
	defer rows.Close()
	return list, rows.Err()
}

// FetchContract returns particular contract.
func (p Provider) FetchContract(ctx *gin.Context, id int64) (*entity.Contract, error) {
	raw := p.db.QueryRowContext(ctx, `
SELECT seller_id, client_id, contract_number, 
	   date_signed, valid_till, credits_initially_issued 
  FROM contract WHERE id = $1`, id)
	var c entity.Contract
	err := raw.Scan(
		&c.SellerID, &c.ClientID, &c.ContractNumber,
		&c.DateSigned, &c.ValidTill, &c.CreditsInitiallyIssued,
	)
	if err != nil {
		return &c, err
	}
	return &c, nil
}

// CreateContract creates a contract.
func (p Provider) CreateContract(ctx *gin.Context, c *entity.Contract) (int64, error) {
	var id int64
	err := p.db.QueryRowContext(ctx, `
INSERT INTO contract (seller_id, client_id, contract_number, date_signed, valid_till, credits_initially_issued) 
	 VALUES ($1, $2, $3, $4, $5, $6)  ON CONFLICT (seller_id, client_id)
  DO UPDATE SET contract_number=EXCLUDED.contract_number RETURNING id`,
		c.SellerID, c.ClientID, c.ContractNumber, c.DateSigned, c.ValidTill, c.CreditsInitiallyIssued).Scan(&id)
	if err != nil {
		return -1, err
	}
	return id, nil
}

// DeleteContract deletes particular contract.
func (p Provider) DeleteContract(ctx *gin.Context, id int64) (int64, error) {
	resp, err := p.db.ExecContext(ctx, `
DELETE FROM contract WHERE id = $1`, id)
	count, err := resp.RowsAffected()
	if err != nil {
		return 0, err
	}
	return count, nil
}
