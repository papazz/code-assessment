package sql

import (
	"testing"

	"github.com/gin-gonic/gin"

	"bitbucket.org/papazz/code-assessment/database"
	"bitbucket.org/papazz/code-assessment/entity"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestProvider_FetchContracts(t *testing.T) {
	ctx := gin.Context{}
	db, err := database.InitPGSQLDB()
	require.NoError(t, err)
	provider := New(db)

	var companyIDAlice int64
	err = provider.db.QueryRowContext(&ctx, `
INSERT INTO company (name, registration_code) 
VALUES ($1, $2) RETURNING id`, "Alice", "Alice").Scan(&companyIDAlice)
	assert.NoError(t, err, "could not insert company Alice")
	assert.NotZero(t, companyIDAlice, "could not get company id Alice")

	var companyIDBob int64
	err = provider.db.QueryRowContext(&ctx, `
INSERT INTO company (name, registration_code) 
VALUES ($1, $2) RETURNING id`, "Bob", "Bob").Scan(&companyIDBob)
	assert.NoError(t, err, "could not insert company Bob")
	assert.NotZero(t, companyIDBob, "could not get company id Bob")

	var contractAliceBobID int64
	err = provider.db.QueryRowContext(&ctx, `
INSERT INTO contract (
	seller_id, client_id, contract_number, 
	date_signed, valid_till, credits_initially_issued) 
VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`,
		companyIDAlice, companyIDBob, "A1B2", "1963-06-19T00:00:00Z", "1963-06-19T00:00:00Z", int64(100)).Scan(&contractAliceBobID)
	assert.NoError(t, err, "could not insert contract Alice<->Bob")
	assert.NotZero(t, contractAliceBobID, "could not get contract Alice<->Bob ID")

	expectedContracts := []entity.Contract{
		{
			SellerID:               companyIDAlice,
			ClientID:               companyIDBob,
			ContractNumber:         "A1B2",
			DateSigned:             "1963-06-19T00:00:00Z",
			ValidTill:              "1963-06-19T00:00:00Z",
			CreditsInitiallyIssued: int64(100),
		},
	}

	fetchedContracts, err := provider.FetchContracts(&ctx)
	assert.NoError(t, err, "could not fetch contracts")
	assert.Equal(t, expectedContracts, fetchedContracts, "contracts mismatch")

	_, err = provider.db.Exec(`DELETE FROM contract WHERE id = $1`, contractAliceBobID)
	assert.NoError(t, err, "could not remove contract")
	_, err = provider.db.Exec(`DELETE FROM company WHERE id = $1`, companyIDAlice)
	assert.NoError(t, err, "could not remove company Alice")
	_, err = provider.db.Exec(`DELETE FROM company WHERE id = $1`, companyIDBob)
	assert.NoError(t, err, "could not remove company Bob")
}
func TestProvider_FetchContract(t *testing.T) {
	ctx := gin.Context{}
	db, err := database.InitPGSQLDB()
	require.NoError(t, err)
	provider := New(db)

	var companyIDAlice int64
	err = provider.db.QueryRowContext(&ctx, `
INSERT INTO company (name, registration_code) 
VALUES ($1, $2) RETURNING id`, "Alice", "Alice").Scan(&companyIDAlice)
	assert.NoError(t, err, "could not insert company Alice")
	assert.NotZero(t, companyIDAlice, "could not get company id Alice")

	var companyIDBob int64
	err = provider.db.QueryRowContext(&ctx, `
INSERT INTO company (name, registration_code) 
VALUES ($1, $2) RETURNING id`, "Bob", "Bob").Scan(&companyIDBob)
	assert.NoError(t, err, "could not insert company Bob")
	assert.NotZero(t, companyIDBob, "could not get company id Bob")

	var contractAliceBobID int64
	err = provider.db.QueryRowContext(&ctx, `
INSERT INTO contract (
	seller_id, client_id, contract_number, 
	date_signed, valid_till, credits_initially_issued) 
VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`,
		companyIDAlice, companyIDBob, "A1B2", "1963-06-19T00:00:00Z", "1963-06-19T00:00:00Z", int64(100)).Scan(&contractAliceBobID)
	assert.NoError(t, err, "could not insert contract Alice<->Bob")
	assert.NotZero(t, contractAliceBobID, "could not get contract Alice<->Bob ID")

	expectedContract := entity.Contract{
		SellerID:               companyIDAlice,
		ClientID:               companyIDBob,
		ContractNumber:         "A1B2",
		DateSigned:             "1963-06-19T00:00:00Z",
		ValidTill:              "1963-06-19T00:00:00Z",
		CreditsInitiallyIssued: int64(100),
	}

	fetchedContract, err := provider.FetchContract(&ctx, contractAliceBobID)
	assert.NoError(t, err, "could not fetch contract")
	assert.Equal(t, expectedContract, *fetchedContract, "contract mismatch")

	_, err = provider.db.Exec(`DELETE FROM contract WHERE id = $1`, contractAliceBobID)
	assert.NoError(t, err, "could not remove contract")
	_, err = provider.db.Exec(`DELETE FROM company WHERE id = $1`, companyIDAlice)
	assert.NoError(t, err, "could not remove company Alice")
	_, err = provider.db.Exec(`DELETE FROM company WHERE id = $1`, companyIDBob)
	assert.NoError(t, err, "could not remove company Bob")
}

func TestProvider_CreateContract(t *testing.T) {
	ctx := gin.Context{}
	db, err := database.InitPGSQLDB()
	require.NoError(t, err)
	provider := New(db)

	var companyIDAlice int64
	err = provider.db.QueryRowContext(&ctx, `
INSERT INTO company (name, registration_code) 
VALUES ($1, $2) RETURNING id`, "Alice", "Alice").Scan(&companyIDAlice)
	assert.NoError(t, err, "could not insert company Alice")
	assert.NotZero(t, companyIDAlice, "could not get company id Alice")

	var companyIDBob int64
	err = provider.db.QueryRowContext(&ctx, `
INSERT INTO company (name, registration_code) 
VALUES ($1, $2) RETURNING id`, "Bob", "Bob").Scan(&companyIDBob)
	assert.NoError(t, err, "could not insert company Bob")
	assert.NotZero(t, companyIDBob, "could not get company id Bob")

	contractAliceBob := entity.Contract{
		SellerID:               companyIDAlice,
		ClientID:               companyIDBob,
		ContractNumber:         "A1B2",
		DateSigned:             "1963-06-19T00:00:00Z",
		ValidTill:              "1963-06-19T00:00:00Z",
		CreditsInitiallyIssued: int64(100),
	}

	createdContractID, err := provider.CreateContract(&ctx, &contractAliceBob)
	assert.NoError(t, err, "could not create contract Alice<->Bob")
	assert.NotZero(t, createdContractID, "contract ID mismatch")

	_, err = provider.db.Exec(`DELETE FROM contract WHERE id = $1`, createdContractID)
	assert.NoError(t, err, "could not remove contract")
	_, err = provider.db.Exec(`DELETE FROM company WHERE id = $1`, companyIDAlice)
	assert.NoError(t, err, "could not remove company Alice")
	_, err = provider.db.Exec(`DELETE FROM company WHERE id = $1`, companyIDBob)
	assert.NoError(t, err, "could not remove company Bob")
}

func TestProvider_DeleteContract(t *testing.T) {
	ctx := gin.Context{}
	db, err := database.InitPGSQLDB()
	require.NoError(t, err)
	provider := New(db)

	var companyIDAlice int64
	err = provider.db.QueryRowContext(&ctx, `
INSERT INTO company (name, registration_code) 
VALUES ($1, $2) RETURNING id`, "Alice", "Alice").Scan(&companyIDAlice)
	assert.NoError(t, err, "could not insert company Alice")
	assert.NotZero(t, companyIDAlice, "could not get company ID Alice")

	var companyIDBob int64
	err = provider.db.QueryRowContext(&ctx, `
INSERT INTO company (name, registration_code) 
VALUES ($1, $2) RETURNING id`, "Bob", "Bob").Scan(&companyIDBob)
	assert.NoError(t, err, "could not insert company Bob")
	assert.NotZero(t, companyIDBob, "could not get company ID Bob")

	var contractAliceBobID int64
	err = provider.db.QueryRowContext(&ctx, `
INSERT INTO contract (
	seller_id, client_id, contract_number, 
	date_signed, valid_till, credits_initially_issued) 
VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`,
		companyIDAlice, companyIDBob, "A1B2", "1963-06-19T00:00:00Z", "1963-06-19T00:00:00Z", int64(100)).Scan(&contractAliceBobID)
	assert.NoError(t, err, "could not insert contract Alice<->Bob")
	assert.NotZero(t, contractAliceBobID, "could not get contract Alice<->Bob ID")

	rowsAffected, err := provider.DeleteContract(&ctx, contractAliceBobID)
	assert.NoError(t, err, "could not delete contract Alice<->Bob")
	assert.Equal(t, int64(1), rowsAffected, "number of deleted contracts mismatch")

	_, err = provider.db.Exec(`DELETE FROM contract WHERE id = $1`, contractAliceBobID)
	assert.NoError(t, err, "could not remove contract")
	_, err = provider.db.Exec(`DELETE FROM company WHERE id = $1`, companyIDAlice)
	assert.NoError(t, err, "could not remove company Alice")
	_, err = provider.db.Exec(`DELETE FROM company WHERE id = $1`, companyIDBob)
	assert.NoError(t, err, "could not remove company Bob")
}
