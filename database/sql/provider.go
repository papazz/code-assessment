package sql

import (
	"database/sql"
)

type (
	// Provider implements entity.Provider to store &
	// access data from storage.
	Provider struct {
		db *sql.DB
	}
)

// New constructs Provider object.
func New(db *sql.DB) Provider {
	return Provider{
		db: db,
	}
}
