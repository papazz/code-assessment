package sql

import (
	"testing"

	"github.com/gin-gonic/gin"

	"bitbucket.org/papazz/code-assessment/database"
	"bitbucket.org/papazz/code-assessment/entity"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestProvider_CreatePurchase(t *testing.T) {
	ctx := gin.Context{}
	db, err := database.InitPGSQLDB()
	require.NoError(t, err)
	provider := New(db)

	var companyIDAlice int64
	err = provider.db.QueryRowContext(&ctx, `
INSERT INTO company (name, registration_code) 
VALUES ($1, $2) RETURNING id`, "Alice", "Alice").Scan(&companyIDAlice)
	assert.NoError(t, err, "could not insert company Alice")
	assert.NotZero(t, companyIDAlice, "could not get company id Alice")

	var companyIDBob int64
	err = provider.db.QueryRowContext(&ctx, `
INSERT INTO company (name, registration_code) 
VALUES ($1, $2) RETURNING id`, "Bob", "Bob").Scan(&companyIDBob)
	assert.NoError(t, err, "could not insert company Bob")
	assert.NotZero(t, companyIDBob, "could not get company id Bob")

	var contractAliceBobID int64
	err = provider.db.QueryRowContext(&ctx, `
INSERT INTO contract (
	seller_id, client_id, contract_number, 
	date_signed, valid_till, credits_initially_issued) 
VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`,
		companyIDAlice, companyIDBob, "A1B2", "1963-06-19T00:00:00Z", "1963-06-19T00:00:00Z", int64(100)).Scan(&contractAliceBobID)
	assert.NoError(t, err, "could not insert contract Alice<->Bob")
	assert.NotZero(t, contractAliceBobID, "could not get contract Alice<->Bob ID")

	purchase := entity.Purchase{
		ContractID:  contractAliceBobID,
		Date:        "1963-06-19T00:00:00Z",
		CreditSpent: int64(111),
	}
	purchaseAliceBobID, err := provider.CreatePurchase(&ctx, &purchase)
	assert.NoError(t, err, "could not create purchase")
	assert.NotZero(t, purchaseAliceBobID, "purchase ID mismatch")

	_, err = provider.db.Exec(`DELETE FROM purchase WHERE id = $1`, purchaseAliceBobID)
	assert.NoError(t, err, "could not remove purchase")
	_, err = provider.db.Exec(`DELETE FROM contract WHERE id = $1`, contractAliceBobID)
	assert.NoError(t, err, "could not remove contract")
	_, err = provider.db.Exec(`DELETE FROM company WHERE id = $1`, companyIDAlice)
	assert.NoError(t, err, "could not remove company Alice")
	_, err = provider.db.Exec(`DELETE FROM company WHERE id = $1`, companyIDBob)
	assert.NoError(t, err, "could not remove company Bob")
}
