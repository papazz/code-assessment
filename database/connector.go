package database

import (
	"database/sql"
	"fmt"
	"os"

	_ "github.com/lib/pq" // including postgres
)

const (
	envPgsqlHost     = "POSTGRES_HOST"
	envPgsqlUser     = "POSTGRES_USER"
	envPgsqlPassword = "POSTGRES_PASSWORD"
	envPgsqlDBName   = "POSTGRES_DB"
	envPgsqlSSLMode  = "POSTGRES_SSL_MODE"
	// MySQL environments
	envMysqlHost     = "MYSQL_HOST"
	envMysqlUser     = "MYSQL_USER"
	envMysqlDBName   = "MYSQL_DB"
	envMysqlPassword = "MYSQL_PASSWORD"
	envMysqlNet      = "MYSQL_NTET"
)

// InitPGSQLDB creates DB connection once
func InitPGSQLDB() (*sql.DB, error) {
	db, err := sql.Open(
		"postgres",
		fmt.Sprintf(
			"host=%s port=%d user=%s password=%s dbname=%s sslmode=%s",
			os.Getenv(envPgsqlHost),
			5432,
			os.Getenv(envPgsqlUser),
			os.Getenv(envPgsqlPassword),
			os.Getenv(envPgsqlDBName),
			os.Getenv(envPgsqlSSLMode),
		))
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return db, err
}

// InitMYSQLDB creates DB connection once
func InitMYSQLDB() (*sql.DB, error) {
	db, err := sql.Open(
		"mysql",
		fmt.Sprintf(
			"%s:%s@%s(%s)/%s",
			os.Getenv(envMysqlUser),
			os.Getenv(envMysqlPassword),
			os.Getenv(envMysqlNet),
			os.Getenv(envMysqlHost),
			os.Getenv(envMysqlDBName),
		))
	if err != nil {
		return nil, err
	}
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	return db, err
}
