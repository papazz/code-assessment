#!/bin/bash -e

/bin/bash ./wait-for-it.sh $POSTGRES_HOST:$POSTGRES_PORT -- 
goose -dir ./migrations postgres "host=$POSTGRES_HOST user=$POSTGRES_USER dbname=$POSTGRES_DB password=$POSTGRES_PASSWORD sslmode=$POSTGRES_SSL_MODE" up
exec ./code-assessment
