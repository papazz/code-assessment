FROM golang:1.11 as builder

WORKDIR $GOPATH/src/bitbucket.org/papazz/code-assessment
ADD https://github.com/golang/dep/releases/download/v0.4.1/dep-linux-amd64 /usr/bin/dep
RUN chmod +x /usr/bin/dep
COPY Gopkg.toml Gopkg.lock ./
RUN dep ensure --vendor-only

RUN go get -u github.com/pressly/goose/cmd/goose
COPY . ./
RUN wget https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh -O ./wait-for-it.sh && \
	chmod +x ./wait-for-it.sh && \
    chmod +x ./entrypoint.sh
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix nocgo -o ./code-assessment .
CMD ["./entrypoint.sh"]
