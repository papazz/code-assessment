package entity

// Contract entity reflection.
type Contract struct {
	SellerID               int64  `json:"seller_id,omitempty" binding:"required"`
	ClientID               int64  `json:"client_id,omitempty" binding:"required"`
	ContractNumber         string `json:"contract_number,omitempty" binding:"required"`
	DateSigned             string `json:"date_signed,omitempty" binding:"required"`
	ValidTill              string `json:"valid_till,omitempty" binding:"required"`
	CreditsInitiallyIssued int64  `json:"credits_initially_issued,omitempty" binding:"required"`
}
