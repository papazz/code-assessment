package entity

// Purchase entity
type Purchase struct {
	ContractID  int64  `json:"contract_id" binding:"required"`
	Date        string `json:"date" binding:"required"`
	CreditSpent int64  `json:"credit_spent" binding:"required"`
}
