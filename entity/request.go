package entity

import (
	"github.com/gin-gonic/gin"
)

type (
	// ServiceBuilder defines interface for constructing code-assessment service controller.
	ServiceBuilder interface {
		Provider(Provider) ServiceBuilder
		Build() (Service, error)
	}
	// Service is used for process companies requests
	Service interface {
		Companies(ctx *gin.Context) ([]Company, error)
		Company(ctx *gin.Context, id int64) (*Company, error)
		CreateCompany(ctx *gin.Context, c *Company) (int64, error)
		DeleteCompany(ctx *gin.Context, id int64) (int64, error)
		Contracts(ctx *gin.Context) ([]Contract, error)
		Contract(ctx *gin.Context, id int64) (*Contract, error)
		CreateContract(ctx *gin.Context, c *Contract) (int64, error)
		DeleteContract(ctx *gin.Context, id int64) (int64, error)
		DoPurchase(ctx *gin.Context, p *Purchase) (int64, error)
	}
	// Provider defines interface to access data from storage.
	Provider interface {
		FetchCompanies(ctx *gin.Context) ([]Company, error)
		FetchCompany(ctx *gin.Context, id int64) (*Company, error)
		CreateCompany(ctx *gin.Context, c *Company) (int64, error)
		DeleteCompany(ctx *gin.Context, id int64) (int64, error)
		FetchContracts(ctx *gin.Context) ([]Contract, error)
		FetchContract(ctx *gin.Context, id int64) (*Contract, error)
		CreateContract(ctx *gin.Context, c *Contract) (int64, error)
		DeleteContract(ctx *gin.Context, id int64) (int64, error)
		CreatePurchase(ctx *gin.Context, p *Purchase) (int64, error)
	}
)
