package entity

// Company entity
type Company struct {
	Name             string `json:"name,omitempty" binding:"required"`
	RegistrationCode string `json:"registration_code,omitempty" binding:"required"`
}
