package util

import (
	"github.com/gin-gonic/gin"
)

// JSONError bulds an error response.
func JSONError(ctx *gin.Context, code int, msg string) {
	ctx.JSON(code, gin.H{
		"error_msg": msg,
		"status":    code,
	})
}

// JSONReturn bulds an successfull response.
func JSONReturn(ctx *gin.Context, code int, data interface{}) {
	ctx.JSON(code, gin.H{
		"data":   data,
		"status": code,
	})
}
