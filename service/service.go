package service

import "bitbucket.org/papazz/code-assessment/entity"

type (
	// Builder is a special type that is capable of building services.
	Builder struct {
		provider entity.Provider
	}
)

// NewBuilder returns loyalty service controller builder.
func NewBuilder() *Builder {
	return new(Builder)
}

// Build builds new service controller according to previously set parameters.
func (b *Builder) Build() (entity.Service, error) {
	return &Controller{
		provider: b.provider,
	}, nil
}

// Provider sets code-assessment provider that will be used to interact with info storage.
func (b *Builder) Provider(p entity.Provider) entity.ServiceBuilder {
	b.provider = p
	return b
}
