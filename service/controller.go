package service

import (
	"fmt"

	"bitbucket.org/papazz/code-assessment/entity"
	"github.com/gin-gonic/gin"
)

// Controller is responsible for code-assessment business-logic flow.
type (
	Controller struct {
		provider entity.Provider
	}
)

// Companies returns list of companies.
func (c *Controller) Companies(ctx *gin.Context) ([]entity.Company, error) {
	list, err := c.provider.FetchCompanies(ctx)
	if err != nil {
		return nil, fmt.Errorf("could not fetch list of companies: %s", err)
	}
	return list, nil
}

// Company returns single company by ID.
func (c *Controller) Company(ctx *gin.Context, id int64) (*entity.Company, error) {
	company, err := c.provider.FetchCompany(ctx, id)
	if err != nil {
		return nil, fmt.Errorf("could not fetch company by ID: %s", err)
	}
	return company, nil
}

// CreateCompany creates new company.
func (c *Controller) CreateCompany(ctx *gin.Context, company *entity.Company) (int64, error) {
	id, err := c.provider.CreateCompany(ctx, company)
	if err != nil {
		return -1, fmt.Errorf("could not create company: %s", err)
	}
	return id, nil
}

// DeleteCompany deletes particular Company.
func (c *Controller) DeleteCompany(ctx *gin.Context, id int64) (int64, error) {
	resp, err := c.provider.DeleteCompany(ctx, id)
	if err != nil {
		return -1, fmt.Errorf("could not delete company: %s", err)
	}
	return resp, nil
}

// Contracts returns list of contracts.
func (c *Controller) Contracts(ctx *gin.Context) ([]entity.Contract, error) {
	list, err := c.provider.FetchContracts(ctx)
	if err != nil {
		return nil, fmt.Errorf("could not fetch list of contracts: %s", err)
	}
	return list, nil
}

// Contract returns single Contract by ID.
func (c *Controller) Contract(ctx *gin.Context, id int64) (*entity.Contract, error) {
	contract, err := c.provider.FetchContract(ctx, id)
	if err != nil {
		return nil, fmt.Errorf("could not fetch contract by ID: %s", err)
	}
	return contract, nil
}

// CreateContract creates new contract.
func (c *Controller) CreateContract(ctx *gin.Context, contract *entity.Contract) (int64, error) {
	id, err := c.provider.CreateContract(ctx, contract)
	if err != nil {
		return -1, fmt.Errorf("could not create contract: %s", err)
	}
	return id, nil
}

// DeleteContract deletes particular contract.
func (c *Controller) DeleteContract(ctx *gin.Context, id int64) (int64, error) {
	resp, err := c.provider.DeleteContract(ctx, id)
	if err != nil {
		return -1, fmt.Errorf("could not delete contract: %s", err)
	}
	return resp, nil
}

// DoPurchase creates new purchase.
func (c *Controller) DoPurchase(ctx *gin.Context, p *entity.Purchase) (int64, error) {
	id, err := c.provider.CreatePurchase(ctx, p)
	if err != nil {
		return -1, fmt.Errorf("could not create purchase: %s", err)
	}
	return id, nil
}
