-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TABLE company(
    id serial PRIMARY KEY,
    name VARCHAR (255) UNIQUE NOT NULL,
    registration_code VARCHAR (255)
);
CREATE DOMAIN alphanum AS VARCHAR(20) CHECK (value ~ '^[A-Z0-9]+$');
CREATE TABLE contract(
    id SERIAL PRIMARY KEY,
    seller_id INTEGER,
    client_id INTEGER,
    contract_number alphanum NOT NULL,
    date_signed DATE NOT NULL,
    valid_till DATE NOT NULL,
    credits_initially_issued INTEGER CHECK (credits_initially_issued > 0),
    CONSTRAINT pk_seller_client_ids UNIQUE (seller_id,client_id),
    CONSTRAINT fk_contract_seller_id FOREIGN KEY (seller_id) REFERENCES company(id) ON DELETE SET NULL,
    CONSTRAINT fk_contract_client_id FOREIGN KEY (client_id) REFERENCES company(id) ON DELETE SET NULL
);
CREATE TABLE IF NOT EXISTS purchase (
    id SERIAL PRIMARY KEY,
    affecting_contract INTEGER,
    purchase_date TIMESTAMP NOT NULL,
    credits_spent INTEGER,
    CONSTRAINT fk_purchase_id FOREIGN KEY (affecting_contract) REFERENCES contract(id) ON DELETE SET NULL
);
-- +goose StatementBegin
CREATE OR REPLACE FUNCTION redeem(
    IN v_contract_id INTEGER,
    IN v_credits_amount INTEGER,
    IN v_purchase_date TIMESTAMP)
  RETURNS INTEGER AS
$BODY$ 
  DECLARE
	current_amount INTEGER;
  BEGIN
  	SELECT credits_initially_issued INTO current_amount 
      FROM contract WHERE id = v_contract_id FOR UPDATE;
  	IF current_amount < v_credits_amount THEN
		RETURN -1;
	ELSE
		UPDATE contract
		   SET credits_initially_issued = (current_amount - abs(v_credits_amount))
		 WHERE id = v_contract_id;
	END IF; 
	INSERT INTO purchase (affecting_contract, purchase_date, credits_spent) 
	     VALUES (v_contract_id, v_purchase_date, v_credits_amount);
  RETURN current_amount - v_credits_amount;
  END;
$BODY$
LANGUAGE plpgsql VOLATILE
-- +goose StatementEnd
-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE IF EXISTS purchase;
DROP TABLE IF EXISTS contract;
DROP TABLE IF EXISTS company;
DROP DOMAIN IF EXISTS alphanum;
DROP FUNCTION IF EXISTS redeem;
