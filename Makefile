.PHONY: install test

install:
	docker pull postgres
test:
	. ./env.sh
	docker run -p 5432:5432 --name some-postgres -e POSTGRES_PASSWORD=${POSTGRES_PASSWORD} -d postgres
	sleep 5
	goose -dir ./migrations postgres "host=${POSTGRES_HOST} user=${POSTGRES_USER} dbname=${POSTGRES_DB} password=${POSTGRES_PASSWORD} sslmode=${POSTGRES_SSL_MODE}" up
	go test ./...
	docker rm -f some-postgres
